
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Game : MonoBehaviour
{
    public static Game game;
    [SerializeField] private int startLives;
    [SerializeField] private TextMeshProUGUI livesText;
    [SerializeField] private TextMeshProUGUI coinsText;
    private int lives;
    private int coins;

    private void Awake()
    {
        if (game == null)
        {
            game = this;
            DontDestroyOnLoad(gameObject);
        }
        else Destroy(gameObject);
    }

    private void Start()
    {
        lives = startLives;
        coins = 0;
        ShowLives(lives);
        ShowCoins(coins);
    }
    public void LoseLive()
    {
        lives--;
        ShowLives(lives);
    }
    public void AddCoins(int amount)
    {
        coins += amount;
        ShowCoins(coins);
    }
    private void ShowLives(int amount)
    {
        livesText.text = amount.ToString();
    }
    private void ShowCoins(int amount)
    {
        coinsText.text = amount.ToString();
    }
}